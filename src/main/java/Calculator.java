import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Calculator {

    private Map<String, Operation> operations = new HashMap<>();

    public Calculator() {
        operations.put("+", BigDecimal::add);
        operations.put("-", BigDecimal::subtract);
        operations.put("*", BigDecimal::multiply);
        operations.put("/", BigDecimal::divide);
    }

    public BigDecimal calculate(BigDecimal a, String sign, BigDecimal b) {
        Operation operation = operations.get(sign);
        if(operation!=null) return operation.calculate(a, b);
        throw new IllegalArgumentException(sign);
    }
}

@FunctionalInterface
interface Operation{
    BigDecimal calculate(BigDecimal a, BigDecimal b);
}