package fxml;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;

public class Controller {
    @FXML private CheckBox checkbox;

    @FXML
    public void triggerCheckBox(ActionEvent event){
        checkbox.setSelected(!checkbox.isSelected());
    }

    @FXML
    public void initialize(){
        checkbox.setSelected(true);
    }

}
