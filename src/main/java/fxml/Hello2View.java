package fxml;

import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.TilePane;

public class Hello2View {
    private TilePane panel = new TilePane();
    private CheckBox checkbox;
    private Button button;

    public TilePane create() {
        preparePanel();
        createLabel();
        createTextField();
        createCheckbox();
        createComboBox();
        return panel;
    }

    private void createButton() {
        button.setOnAction(event -> checkbox.setSelected(!checkbox.isSelected()));
        button = new Button("My second button");
        panel.getChildren().add(button);
    }

    private void createComboBox() {
        ComboBox<String> comboBox = new ComboBox<>();
        comboBox.setValue("Arabia Saudyjska");
        comboBox.setItems(FXCollections.observableArrayList("Rosja", "Arabia Saudyjska"));
        panel.getChildren().add(comboBox);
    }

    private void createCheckbox() {
        checkbox = new CheckBox("My second checkbox");
        panel.getChildren().add(checkbox);
    }

    private void createTextField() {
        panel.getChildren().add(new TextField("My second textfiled"));
    }

    private void createLabel() {
        panel.getChildren().add(new Label("My second label"));
    }

    private void preparePanel() {
        panel.setHgap(24);
        panel.setVgap(24);
        panel.setAlignment(Pos.CENTER);
    }


}
