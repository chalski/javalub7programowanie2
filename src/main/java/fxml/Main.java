package fxml;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;

import java.math.BigDecimal;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        TabPane tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        Parent root = FXMLLoader.load(getClass().getResource("/sample.fxml"));
        tabPane.getTabs().add(new Tab("FXML", root));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(tabPane, 300, 275));
        primaryStage.show();

        tabPane.getTabs().add(new Tab("JavaFx", new Hello2View().create()));
        TilePane productEditPanel = new ProductEditView(
                new ProductEditModel(
                        new Product("produkt", BigDecimal.TEN)
                )
        ).create();

        tabPane.getTabs().add(new Tab("Produkt", productEditPanel));
    }


    public static void main(String[] args) {
        launch(args);
    }
}
