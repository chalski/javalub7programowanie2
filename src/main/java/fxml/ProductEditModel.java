package fxml;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ProductEditModel {

    private Product product;
    private StringProperty name;
    private StringProperty price;

    public ProductEditModel(Product product) {
        this.product = product;
        this.name = new SimpleStringProperty(product.getName());
        this.price = new SimpleStringProperty(product.getPrice().toString());
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getPrice() {
        return price.get();
    }

    public StringProperty priceProperty() {
        return price;
    }

    public void setPrice(String price) {
        this.price.set(price);
    }
}
