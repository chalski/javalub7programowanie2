package fxml;

import javafx.scene.control.TextField;
import javafx.scene.layout.TilePane;

public class ProductEditView {

    private ProductEditModel model;

    public ProductEditView(ProductEditModel model) {
        this.model = model;
    }

    public TilePane create(){
        TilePane panel = new TilePane();
        TextField textField = new TextField();
        textField.textProperty().bindBidirectional(model.nameProperty());
        panel.getChildren().add(textField);

        TextField textField2 = new TextField();
        textField2.textProperty().bind(model.nameProperty());
        panel.getChildren().add(textField2);
        return panel;
    }

}
