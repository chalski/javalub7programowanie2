package regexp;

import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CardNumberValidator {
    public boolean validate(String cardNumber) {
        boolean isValid = true;
        char[] chars = cardNumber.toCharArray();
        if (chars.length == 16) {
            return StringUtils.isNumeric(cardNumber);
        }
        if(chars.length!=19) return false;

        isValid = isValid && CharUtils.isAsciiNumeric(chars[0]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[1]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[2]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[3]);
        isValid = isValid && (chars[4] == ' ' || chars[4] == '-');
        isValid = isValid && CharUtils.isAsciiNumeric(chars[5]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[6]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[7]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[8]);
        isValid = isValid && (chars[9] == ' ' || chars[9] == '-');
        isValid = isValid && CharUtils.isAsciiNumeric(chars[10]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[11]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[12]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[13]);
        isValid = isValid && (chars[14] == ' ' || chars[14] == '-');
        isValid = isValid && CharUtils.isAsciiNumeric(chars[15]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[16]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[17]);
        isValid = isValid && CharUtils.isAsciiNumeric(chars[18]);
        return isValid;
    }




    private String regexp = "\\d{4}[ -]{0,1}\\d{4}[ -]?\\d{4}[ -]?\\d{4}";
    private Pattern pattern = Pattern.compile(regexp);

    public boolean regexValidate(String cardNumber) {
        Matcher matcher = pattern.matcher(cardNumber);
        return matcher.matches();
    }
}
