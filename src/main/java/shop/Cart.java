package shop;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class Cart {

    private Integer id;
    private List<Product> products;
    private PaymentMethod paymentMethod;


    public BigDecimal overallPrice(){
        return products.stream().map(Product::getPrice).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
    }


}
