package shop;

public enum PaymentMethod {
    CASH,
    CARD
}
