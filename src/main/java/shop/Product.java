package shop;

import lombok.*;

import java.math.BigDecimal;
import java.time.Period;

@Builder
@ToString
@EqualsAndHashCode
@Data
@AllArgsConstructor
public class Product {

    private final Integer id;
    private final String name;
    private BigDecimal price;
    private ProductCategory category;
    private Period availability;

}
