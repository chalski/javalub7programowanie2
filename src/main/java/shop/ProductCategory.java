package shop;

public enum ProductCategory {
    ELECTRONIC,
    HOME,
    OFFICE,
    OTHER
}
