package shop.discountCalculator;

import shop.Cart;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class DiscountCalculator {

    private List<DiscountStrategy> discountStrategies = new ArrayList<>();

    public DiscountCalculator() {
        discountStrategies.add(new FirstDiscountStrategy());
        discountStrategies.add(new SecondDiscountStrategy());
        discountStrategies.add(new ThirdDiscountStrategy());
    }

    public BigDecimal calculate(Cart cart) {
        BigDecimal discount = BigDecimal.ZERO;
        for(DiscountStrategy strategy: discountStrategies){
            if(strategy.isAcceptable(cart)){
                discount = discount.add(strategy.compute(cart));
            }
        }
        return discount;
    }

}