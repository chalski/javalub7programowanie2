package shop.discountCalculator;

import shop.Cart;

import java.math.BigDecimal;

public interface DiscountStrategy {

    boolean isAcceptable(Cart cart);

    BigDecimal compute(Cart cart);

}
