package shop.discountCalculator;

import shop.Cart;

import java.math.BigDecimal;

public class FirstDiscountStrategy implements DiscountStrategy {

    private static final BigDecimal FIRST_DISCOUNT_VALUE = BigDecimal.valueOf(1000);
    private static final BigDecimal FIRST_DISCOUNT_PERCENT = BigDecimal.valueOf(0.1);

    @Override
    public boolean isAcceptable(Cart cart) {
        return cart.overallPrice().compareTo(FIRST_DISCOUNT_VALUE) >= 0;
    }

    @Override
    public BigDecimal compute(Cart cart) {
        return cart.overallPrice().multiply(FIRST_DISCOUNT_PERCENT);
    }
}
