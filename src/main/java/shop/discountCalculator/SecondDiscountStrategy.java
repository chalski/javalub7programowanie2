package shop.discountCalculator;

import shop.Cart;

import java.math.BigDecimal;

public class SecondDiscountStrategy implements DiscountStrategy {

    private static final BigDecimal SECOND_DISCOUNT_PERCENT = BigDecimal.valueOf(0.05);
    private static final int SECOND_DISCOUNT_PRODUCT_COUNT = 20;

    @Override
    public boolean isAcceptable(Cart cart) {
        return cart.getProducts().size() >= SECOND_DISCOUNT_PRODUCT_COUNT;
    }

    @Override
    public BigDecimal compute(Cart cart) {
        return cart.overallPrice().multiply(SECOND_DISCOUNT_PERCENT);
    }

}
