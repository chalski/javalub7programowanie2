package shop.discountCalculator;

import shop.Cart;
import shop.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static shop.ProductCategory.ELECTRONIC;

public class ThirdDiscountStrategy implements DiscountStrategy{

    private static final BigDecimal THIRD_DISCOUNT_PERCENT = BigDecimal.valueOf(0.5);
    private static final int THIRD_DISCOUNT_PRODUCT_COUNT = 2;

    @Override
    public boolean isAcceptable(Cart cart) {
        List<Product> electronicProducts = getElectronicProducts(cart);
        int electronicProductCount = electronicProducts.size();
        return electronicProductCount >= THIRD_DISCOUNT_PRODUCT_COUNT;
    }

    @Override
    public BigDecimal compute(Cart cart) {
        BigDecimal discount = BigDecimal.ZERO;
        List<Product> electronicProducts = getElectronicProducts(cart);
        sortProductList(electronicProducts);
        //        Collections.sort(electronicProducts, Comparator.comparing(Product::getPrice));

        int electronicProductCount = electronicProducts.size();
        int productsWithDiscountCount = electronicProductCount / 2;
        for(int i = 0; i < productsWithDiscountCount; i++){
            Product product = electronicProducts.get(i);
            BigDecimal price = product.getPrice();
            BigDecimal partialDiscount = price.multiply(THIRD_DISCOUNT_PERCENT);
            discount = discount.add(partialDiscount);
        }

        return discount;
    }

    private void sortProductList(List<Product> electronicProducts) {
        electronicProducts.sort(new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return o1.getPrice().compareTo(o2.getPrice());
            }
        });
    }

    private List<Product> getElectronicProducts(Cart cart) {
        List<Product> electronicProducts = new ArrayList<>();
        for (Product product : cart.getProducts()) {
            if (ELECTRONIC == product.getCategory()) {
                electronicProducts.add(product);
            }
        }
        return electronicProducts;
    }
}
