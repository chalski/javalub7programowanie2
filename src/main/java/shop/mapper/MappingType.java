package shop.mapper;

public enum MappingType {
    XML, JSON, CUSTOM
}
