package shop.mapper;

import shop.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductMappingFactory {

    private List<ProductMappingStrategy> strategies = new ArrayList<>();

    public ProductMappingFactory(){
        strategies.add(new XMLMappingStrategy());
        strategies.add(new JsonMappingStrategy());
        strategies.add(new CustomMappingStrategy());
    }

    public String map(Product product, MappingType type) {
        ProductMappingStrategy chosenStrategy = new EmptyMappingStrategy();
        for(ProductMappingStrategy strategy: strategies){
            if(strategy.isAcceptable(type)){
                chosenStrategy = strategy;
            }
        }
        return chosenStrategy.map(product);
    }

    public String mapFunctional(Product product, MappingType type){
        return strategies.stream()
                .filter(strategy -> strategy.isAcceptable(type))
                .findFirst()
                .orElse(new EmptyMappingStrategy())
                .map(product);
    }

}