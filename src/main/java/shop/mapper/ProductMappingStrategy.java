package shop.mapper;

import shop.Product;

public interface ProductMappingStrategy {

    boolean isAcceptable(MappingType type);

    String map(Product product);

}

class XMLMappingStrategy implements ProductMappingStrategy{

    @Override
    public boolean isAcceptable(MappingType type) {
        return type == MappingType.XML;
    }

    @Override
    public String map(Product product) {
        return "<product>" +
                "<id>" + product.getId() + "</id>" +
                "<name>" + product.getName() + "</name>" +
                "<category>" + product.getCategory() + "</category>" +
                "</product>";
    }
}

class JsonMappingStrategy implements ProductMappingStrategy{

    @Override
    public boolean isAcceptable(MappingType type) {
        return type == MappingType.JSON;
    }

    @Override
    public String map(Product product) {
        return "{" +
                "id:" + product.getId() + "," +
                "name:" + product.getName() + "," +
                "category:" + product.getCategory() +
                "}";
    }
}

class CustomMappingStrategy implements ProductMappingStrategy{

    @Override
    public boolean isAcceptable(MappingType type) {
        return type == MappingType.CUSTOM;
    }

    @Override
    public String map(Product product) {
        return String.valueOf(product.getId()) + "," +
                product.getName() + "," +
                product.getCategory();
    }
}

class EmptyMappingStrategy implements ProductMappingStrategy{

    @Override
    public boolean isAcceptable(MappingType type) {
        return true;
    }

    @Override
    public String map(Product product) {
        return "";
    }
}