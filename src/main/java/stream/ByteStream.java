package stream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ByteStream {

    public static void main(String[] args) {
        File targetFile = new File("E:\\WORKSPACE\\sda\\javalub7\\javalub7programowanie2\\sample\\example2-target.bin");
        File targetFile2 = new File("E:\\WORKSPACE\\sda\\javalub7\\javalub7programowanie2\\sample\\example2-copy.bin");


        try(
                FileInputStream fileInputStream = new FileInputStream(targetFile);
                FileOutputStream fileOutputStream = new FileOutputStream(targetFile2)
        ) {
            int i;
            while ((i = fileInputStream.read())!=-1){
                fileOutputStream.write(i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
