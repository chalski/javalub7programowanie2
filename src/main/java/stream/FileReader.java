package stream;

import java.io.*;

public class FileReader {

    public static void main(String[] args) {

        File file = new File("E:\\WORKSPACE\\sda\\javalub7\\javalub7programowanie2\\sample\\example1.txt");
        File targetFile = new File("E:\\WORKSPACE\\sda\\javalub7\\javalub7programowanie2\\sample\\example1-target.txt");

        String prefix = "Kopia pliku " + file.getName() + ":\n";
        try (
                BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(targetFile)));
        ) {

            for (char c : prefix.toCharArray()) {
                writer.write(c);
            }
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                writer.write(line);
                writer.write("\r");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
