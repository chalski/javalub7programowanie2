package stream;

import lombok.AllArgsConstructor;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class Streamy {

    public static void main(String[] args) {

        MyObject my_object = new MyObject("My object", 123l, Arrays.asList(new Child("My object 2")));

        File targetFile = new File("E:\\WORKSPACE\\sda\\javalub7\\javalub7programowanie2\\sample\\example2-target.bin");

        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(targetFile))){
            outputStream.writeObject(my_object);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Object o = null;
        try(ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(targetFile))){
            o = inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}

@AllArgsConstructor
class MyObject implements Serializable{

    private static final long serialVersionUID = 2;

    String name;
    float i;
    transient List<Child> children;
}

@AllArgsConstructor
class Child{
    String childName;
}
