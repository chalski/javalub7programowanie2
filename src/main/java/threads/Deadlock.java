package threads;

import java.util.ArrayList;
import java.util.List;

public class Deadlock {

    public static void main(String[] args) {
        List<String> strings1 = new ArrayList<>();
        List<String> strings2 = new ArrayList<>();

        new Thread(() -> {
            synchronized (strings1) {
                System.out.println("Wątek 1 - Wziąłem strings1");

                sleep();
                System.out.println("Wątek 1 - Czekam na strings2");
                synchronized (strings2){
                    System.out.println("Wątek 1 - Wziąłem strings2");
                }
            }
        }).start();
        new Thread(() -> {
            synchronized (strings2) {
                System.out.println("Wątek 2 - Wziąłem strings2");

                sleep();
                System.out.println("Wątek 2 - Czekam na strings1");
                synchronized (strings1){
                    System.out.println("Wątek 2 - Wziąłem strings1");
                }
            }
        }).start();


    }

    private static void sleep() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
