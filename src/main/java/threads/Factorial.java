package threads;

import lombok.AllArgsConstructor;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Factorial {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();

        BigInteger factorial = factorialMultiThread(500_000, 16);

        System.out.println(factorial);
        long time = System.currentTimeMillis() - start;
        System.out.println("Computed in " + time + " ms.");
    }

    private static BigInteger factorial(int value) {
        return factorialPart(1, value);
    }

    static BigInteger factorialPart(int start, int end) {
        BigInteger factorial = BigInteger.ONE;
        for (int i = start; i <= end; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }

    private static BigInteger factorialMultiThread(int value, int threads) throws ExecutionException, InterruptedException {
        List<Integer> ranges = getRanges(value, threads);
        List<Future<BigInteger>> factorialParts = getFactorialParts(threads, ranges);
        return factorialParts.stream()
                .map(Factorial::getValue)
                .reduce(BigInteger::multiply)
                .orElse(BigInteger.ZERO);
    }

    private static List<Future<BigInteger>> getFactorialParts(int threads, List<Integer> ranges) {
        List<Future<BigInteger>> factorialParts = new ArrayList<>();
        ExecutorService executorService = Executors.newFixedThreadPool(threads);
        for (int i = 0; i < threads; i++) {
            int startRange = ranges.get(i) + 1;
            int endRange = ranges.get(i + 1);
            factorialParts.add(
                    executorService.submit(new FactorialPartComputer(i, startRange, endRange))
            );

        }
        executorService.shutdown();
        return factorialParts;
    }

    private static List<Integer> getRanges(int value, int threads) {
        int rangeSize = value / threads;
        List<Integer> ranges = new ArrayList<>();
        ranges.add(0);
        for (int i = 1; i < threads; i++) {
            ranges.add(i * rangeSize);
        }
        ranges.add(value);
        return ranges;
    }

    private static BigInteger getValue(Future<BigInteger> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return BigInteger.ZERO;
    }
}

@AllArgsConstructor
class FactorialPartComputer implements Callable<BigInteger>{

    private int threadNumber;
    private int startRange;
    private int endRange;

    @Override
    public BigInteger call() throws Exception {
        System.out.println("Thread " + threadNumber + " started from " + startRange + " to " + endRange);
        BigInteger result = Factorial.factorialPart(startRange, endRange);
        System.out.println("Thread " + threadNumber + " finished from " + startRange + " to " + endRange);
        return result;
    }
}
