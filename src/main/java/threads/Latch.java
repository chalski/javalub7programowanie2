package threads;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Latch {

    public static void main(String[] args) {
        CountDownLatch latch = new CountDownLatch(2);
        Lock lock = new ReentrantLock();

        new Thread(()->{
            System.out.println("Third thread start");
            System.out.println("I should wait for first and second thread");

            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Third thread ends");
        }).start();

        new Thread(()->{
            System.out.println("First thread start");
            sleep(100);
            latch.countDown();
            System.out.println("First thread ends");
        }).start();
        new Thread(()->{
            System.out.println("Second thread start");
            sleep(150);
            latch.countDown();
            System.out.println("Second thread ends");
        }).start();


    }

    private static void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
