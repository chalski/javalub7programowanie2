package threads;

import java.util.ArrayList;
import java.util.List;

public class Synchronized {

    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();

        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (strings) {
                    strings.add("String");
                }
            }
        }).start();
        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (strings) {
                    System.out.println(strings);
                }
            }
        }).start();
    }

}
