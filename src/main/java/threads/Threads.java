package threads;

public class Threads {

    public static void main(String[] args) {
        new Thread(new RunnableThread()).start();
        new ThreadThread().start();
        Threads.print();
    }

    public static void print() {
        for (int i = 0; i <= 1000; i++) {
            System.out.println(Thread.currentThread().getName() + ": " + i);
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

class RunnableThread implements Runnable {

    @Override
    public void run() {
        Threads.print();
    }

}

class ThreadThread extends Thread {

    @Override
    public void run() {
        Threads.print();
    }
}
