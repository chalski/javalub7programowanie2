import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.math.BigDecimal;

public class CalculatorTest {

    private BigDecimal a = BigDecimal.valueOf(8);
    private BigDecimal b = BigDecimal.valueOf(2);
    private Calculator calculator = new Calculator();

    @Test
    public void shouldAddTwoNumbers() {
        BigDecimal result = calculator.calculate(a, "+", b);

        Assertions.assertThat(result).isEqualByComparingTo("10");
    }

    @Test
    public void shouldSubstractTwoNumbers() {
        BigDecimal result = calculator.calculate(a, "-", b);

        Assertions.assertThat(result).isEqualByComparingTo("6");
    }

    @Test
    public void shouldMultiplyTwoNumbers() {
        BigDecimal result = calculator.calculate(a, "*", b);

        Assertions.assertThat(result).isEqualByComparingTo("16");
    }

    @Test
    public void shouldDivideTwoNumbers() {
        BigDecimal result = calculator.calculate(a, "/", b);

        Assertions.assertThat(result).isEqualByComparingTo("4");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenSignIsNotRecognized(){
        calculator.calculate(BigDecimal.ZERO,"unrecognized", BigDecimal.ONE);
    }



}