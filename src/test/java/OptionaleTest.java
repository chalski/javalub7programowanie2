import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import shop.Cart;
import shop.PaymentMethod;
import shop.Product;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

public class OptionaleTest {

    Optional<BigDecimal> empty = Optional.ofNullable(null);
    Optional<BigDecimal> filled = Optional.of(BigDecimal.TEN);

    @Test
    public void isPresent(){
        Assert.assertFalse(empty.isPresent());
        Assert.assertTrue(filled.isPresent());
    }

    @Test
    public void isPresentAndGet(){
        if(filled.isPresent()){
            Assertions.assertThat(filled.get()).isEqualByComparingTo("10");
        }
    }

    @Test(expected = NoSuchElementException.class)
    public void getOnNull(){
        empty.get();
    }

    @Test
    public void ifPresent(){
        empty.ifPresent(a -> {
            Assert.fail();
        });
        filled.ifPresent(System.out::println);
    }

    @Test
    public void orElse(){
        Assertions.assertThat(empty.orElse(BigDecimal.ZERO)).isEqualByComparingTo("0");
        Assertions.assertThat(filled.orElse(BigDecimal.ZERO)).isEqualByComparingTo("10");
    }

    @Test(expected = IllegalArgumentException.class)
    public void orElseThrow(){
        filled.orElseThrow(IllegalStateException::new);
        empty.orElseThrow(IllegalArgumentException::new);
    }

    @Test
    public void mapInOptional() {
        Cart cart = new Cart(null, Arrays.asList(new Product(null, "Nazwa", null, null, null)), PaymentMethod.CARD);

        if (cart != null) {
            List<Product> products = cart.getProducts();
            if (products != null) {
                Product product = products.get(0);
                if (product != null) {
                    System.out.println(product.getName());
                }
            }
        }

        Optional.ofNullable(cart)
                .map(Cart::getProducts)
                .map(products -> products.get(0))
                .map(Product::getName)
                .ifPresent(System.out::println);
    }

}