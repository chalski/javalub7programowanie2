package regexp;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class RegexpTest {

    private CardNumberValidator validator = new CardNumberValidator();
    private String cardNumber = "1234 5678 9012 3456";
    private String alternativeCardNumber1 = "1234-5678-9012-3456";
    private String alternativeCardNumber2 = "1234567890123456";
    private String wrongNumber1 = "1234 5678 9012 34";
    private String wrongNumber2 = "1234 5678 9012 34as";
    private String wrongNumber3 = "Kowalski";
    private String wrongNumber4 = "nr=1234 5678 9012 3456";

    @Test
    public void parseCardNumberOldWay() {
        Assertions.assertThat(validator.validate(cardNumber)).isTrue();
        Assertions.assertThat(validator.validate(alternativeCardNumber1)).isTrue();
        Assertions.assertThat(validator.validate(alternativeCardNumber2)).isTrue();
        Assertions.assertThat(validator.validate(wrongNumber1)).isFalse();
        Assertions.assertThat(validator.validate(wrongNumber2)).isFalse();
        Assertions.assertThat(validator.validate(wrongNumber3)).isFalse();
        Assertions.assertThat(validator.validate(wrongNumber4)).isFalse();
    }

    @Test
    public void parseCardNumberByRegexp() {
        Assertions.assertThat(validator.regexValidate(cardNumber)).isTrue();
        Assertions.assertThat(validator.regexValidate(alternativeCardNumber1)).isTrue();
        Assertions.assertThat(validator.regexValidate(alternativeCardNumber2)).isTrue();
        Assertions.assertThat(validator.regexValidate(wrongNumber1)).isFalse();
        Assertions.assertThat(validator.regexValidate(wrongNumber2)).isFalse();
        Assertions.assertThat(validator.regexValidate(wrongNumber3)).isFalse();
        Assertions.assertThat(validator.regexValidate(wrongNumber4)).isFalse();
    }


}
