package shop.discountCalculator;

import org.junit.Test;
import shop.Cart;
import shop.PaymentMethod;
import shop.Product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static shop.ProductCategory.ELECTRONIC;
import static shop.ProductCategory.HOME;
import static shop.ProductCategory.OFFICE;

public class DiscountCalculatorTest {

    private DiscountCalculator discountCalculator = new DiscountCalculator();

    @Test
    public void shouldGiveDiscountWhenCartPriceIsGreaterThan1000() {
        List<Product> products = new ArrayList<>();
        products.add(Product.builder().price(BigDecimal.valueOf(1000)).build());
        Cart cart = new Cart(123, products, PaymentMethod.CARD);

        BigDecimal discount = discountCalculator.calculate(cart);

        assertThat(discount).isEqualByComparingTo("100");
    }

    @Test
    public void shouldNotGiveDiscountWhenCartPriceIsLessThan1000() {
        List<Product> products = new ArrayList<>();
        products.add(Product.builder().price(BigDecimal.valueOf(800)).build());
        Cart cart = new Cart(123, products, PaymentMethod.CARD);

        BigDecimal discount = discountCalculator.calculate(cart);

        assertThat(discount).isEqualByComparingTo("0");
    }

    @Test
    public void shouldGiveDiscountWhenThereIsMoreThan20ProductsInCart() {
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            products.add(Product.builder().price(BigDecimal.valueOf(10)).build());
        }
        Cart cart = new Cart(123, products, PaymentMethod.CARD);

        BigDecimal discount = discountCalculator.calculate(cart);

        assertThat(discount).isEqualByComparingTo("10");
    }

    @Test
    public void shouldGiveDiscountWhenThereIsMoreThan20ProductsAndValueIsGreaterThan1000InCart() {
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            products.add(Product.builder().price(BigDecimal.valueOf(100)).build());
        }
        Cart cart = new Cart(123, products, PaymentMethod.CARD);

        BigDecimal discount = discountCalculator.calculate(cart);

        assertThat(discount).isEqualByComparingTo("300");
    }

    @Test
    public void shouldNotGiveDiscountWhenThereIsOnlyOneElectronicProduct() {
        List<Product> products = new ArrayList<>();
        products.add(Product.builder().price(BigDecimal.valueOf(100)).category(ELECTRONIC).build());
        Cart cart = new Cart(123, products, PaymentMethod.CARD);

        BigDecimal discount = discountCalculator.calculate(cart);

        assertThat(discount).isEqualByComparingTo("0");
    }

    @Test
    public void shouldGiveDiscountWhenThereAreTwoElectronicProducts() {
        List<Product> products = new ArrayList<>();
        products.add(Product.builder().price(BigDecimal.valueOf(100)).category(ELECTRONIC).build());
        products.add(Product.builder().price(BigDecimal.valueOf(100)).category(ELECTRONIC).build());
        Cart cart = new Cart(123, products, PaymentMethod.CARD);

        BigDecimal discount = discountCalculator.calculate(cart);

        assertThat(discount).isEqualByComparingTo("50");
    }

    @Test
    public void shouldGiveDiscountWhenThereAreThreeElectronicProducts() {
        List<Product> products = new ArrayList<>();
        products.add(Product.builder().price(BigDecimal.valueOf(300)).category(ELECTRONIC).build());
        products.add(Product.builder().price(BigDecimal.valueOf(200)).category(ELECTRONIC).build());
        products.add(Product.builder().price(BigDecimal.valueOf(100)).category(ELECTRONIC).build());
        Cart cart = new Cart(123, products, PaymentMethod.CARD);

        BigDecimal discount = discountCalculator.calculate(cart);

        assertThat(discount).isEqualByComparingTo("50");
    }

    @Test
    public void shouldNotGiveDiscountWhenThereAreMoreProductButOnlyOneElectronic() {
        List<Product> products = new ArrayList<>();
        products.add(Product.builder().price(BigDecimal.valueOf(300)).category(HOME).build());
        products.add(Product.builder().price(BigDecimal.valueOf(200)).category(ELECTRONIC).build());
        products.add(Product.builder().price(BigDecimal.valueOf(100)).category(OFFICE).build());
        Cart cart = new Cart(123, products, PaymentMethod.CARD);

        BigDecimal discount = discountCalculator.calculate(cart);

        assertThat(discount).isEqualByComparingTo("0");
    }

    @Test
    public void shouldGiveManyDiscounts() {
        List<Product> products = new ArrayList<>();
        products.add(Product.builder().price(BigDecimal.valueOf(300)).category(ELECTRONIC).build());
        products.add(Product.builder().price(BigDecimal.valueOf(200)).category(ELECTRONIC).build());
        for (int i = 0; i < 20; i++) {
            products.add(Product.builder().price(BigDecimal.valueOf(100)).category(OFFICE).build());
        }

        Cart cart = new Cart(123, products, PaymentMethod.CARD);

        BigDecimal discount = discountCalculator.calculate(cart);

        assertThat(discount).isEqualByComparingTo("475");
    }

    @Test
    public void shouldGiveManyDiscounts2() {
        List<Product> products = new ArrayList<>();
        products.add(Product.builder().price(BigDecimal.valueOf(500)).category(ELECTRONIC).build());
        products.add(Product.builder().price(BigDecimal.valueOf(500)).category(ELECTRONIC).build());
        products.add(Product.builder().price(BigDecimal.valueOf(100)).category(OFFICE).build());

        Cart cart = new Cart(123, products, PaymentMethod.CARD);

        BigDecimal discount = discountCalculator.calculate(cart);

        assertThat(discount).isEqualByComparingTo("360");
    }


}