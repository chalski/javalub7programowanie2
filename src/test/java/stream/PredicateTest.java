package stream;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import shop.Product;
import shop.ProductCategory;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PredicateTest {

    private List<String> strings = Arrays.asList("Ala", "ma", "kota", "a", "jej", "mama", "nawet", "dwa");

    @Test
    public void filter() {
        List<String> result = strings.stream()
                .filter(word -> word.length() > 3)
                .collect(Collectors.toList());

        Assertions.assertThat(result).containsExactlyInAnyOrder("kota", "mama", "nawet");
    }

    @Test
    public void filterNull() {
        List<String> strings = Arrays.asList("String", null, null, "String 2");

        List<String> result = strings.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        Assertions.assertThat(result).containsExactlyInAnyOrder("String", "String 2");
    }

    @Test
    public void anyMatch() {
        List<String> strings1 = Arrays.asList("String", null, null, "String 2");
        List<String> strings2 = Arrays.asList("String", "String 2");

        boolean result1 = strings1.stream()
                .anyMatch(Objects::isNull);
        boolean result2 = strings2.stream()
                .anyMatch(word -> word == null);

        Assertions.assertThat(result1).isTrue();
        Assertions.assertThat(result2).isFalse();
    }

    @Test
    public void allMatch() {
        List<String> strings1 = Arrays.asList("String", null, null, "String 2");
        List<String> strings2 = Arrays.asList("String", "String 2");

        boolean result1 = strings1.stream()
                .allMatch(Objects::nonNull);
        boolean result2 = strings2.stream()
                .allMatch(word -> word != null);

        Assertions.assertThat(result1).isFalse();
        Assertions.assertThat(result2).isTrue();
    }

    @Test
    public void noneMatch() {
        List<String> strings1 = Arrays.asList("String", null, null, "String 2");
        List<String> strings2 = Arrays.asList("String", "String 2");

        boolean result1 = strings1.stream()
                .noneMatch(word -> word == null);
        boolean result2 = strings2.stream()
                .noneMatch(word -> word == null);

        Assertions.assertThat(result1).isFalse();
        Assertions.assertThat(result2).isTrue();
    }

    @Test
    public void forEach() {
        strings.stream()
                .filter(word -> word.length() > 3)
                .forEach(System.out::println);
    }

    @Test
    public void peek() {
        strings.stream()
                .peek(word -> System.out.println("Wszystkie: " + word))
                .filter(word -> word.length() > 3)
                .peek(word -> System.out.println(">3: " + word))
                .filter(word -> word.length() < 5)
                .peek(word -> System.out.println("<5: " + word))
                .collect(Collectors.toList());
    }

    @Test
    public void map() {
        List<Integer> collect = strings.stream()
                .map(String::length)
                .collect(Collectors.toList());
        Assertions.assertThat(collect).containsExactly(3, 2, 4, 1, 3, 4, 5, 3);
    }

    @Test
    public void flatMap() {
        List<List<String>> strings = Arrays.asList(
                Arrays.asList("Ala", "ma", "kota"),
                Arrays.asList("Olek", "ma", "psa")
        );

        List<String> collect = strings.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        Assertions.assertThat(collect).containsExactlyInAnyOrder("Ala", "ma", "kota", "Olek", "ma", "psa");
    }

    @Test
    public void reduce1() {
        Integer result = strings.stream()
                .map(String::length)
                .reduce((a, b) -> a + b)
                .get();

        Assertions.assertThat(result).isEqualTo(25);
    }

    @Test
    public void reduce2() {
        Integer result = strings.stream()
                .map(String::length)
                .reduce((a, b) -> a > b ? a : b)
                .get();

        Assertions.assertThat(result).isEqualTo(5);
    }

    @Test
    public void reduce3() {
        Stream<Integer> of = Stream.of();
        Integer result = of
                .reduce((a, b) -> a > b ? a : b)
                .orElse(0);

        Assertions.assertThat(result).isEqualTo(0);
    }

    @Test
    public void reduce4() {
        Integer result = strings.stream()
                .map(String::length)
                .reduce(-10, (a, b) -> a + b);

        Assertions.assertThat(result).isEqualTo(15);
    }

    @Test
    public void reduce5() {
        Integer result = strings.stream()
                .parallel()
                .map(String::length)
                .reduce(0, (sum, b) -> {
                            System.out.println("Accumulator "+ Thread.currentThread().getName()+":" + sum + ", " + b);
                            return sum + b;
                        },
                        (sum1, sum2) -> {
                            System.out.println("Combiner "+ Thread.currentThread().getName()+":" + sum1 + ", " + sum2);
                            return sum1 + sum2;
                        });

        Assertions.assertThat(result).isEqualTo(25);
    }

    @Test
    public void collectorsToSet() {

        Set<String> words = Stream.of("Ala", "Ala", "ma", "kota")
                .collect(Collectors.toSet());

        Assertions.assertThat(words).containsExactlyInAnyOrder("Ala", "ma", "kota");
    }

    @Test
    public void collectorsToMap() {
        Map<String, Integer> result = strings.stream()
                .collect(Collectors.toMap(
                        word -> word,
                        String::length
                ));

        Assertions.assertThat(result)
                .containsEntry("Ala", 3)
                .containsEntry("ma", 2)
                .containsEntry("kota", 4)
                .containsEntry("a", 1)
                .containsEntry("jej", 3)
                .containsEntry("mama", 4)
                .containsEntry("nawet", 5)
                .containsEntry("dwa", 3);
    }

    @Test
    public void collectorsToTreeMap() {
        TreeMap<String, Integer> result = strings.stream()
                .collect(Collectors.toMap(
                        word -> word,
                        String::length,
                        (Integer length1, Integer length2) -> length1,
                        TreeMap::new
                ));
    }

    @Test
    public void collectorsJoining() {
        System.out.println(strings.stream().collect(Collectors.joining()));
        System.out.println(strings.stream().collect(Collectors.joining(", ")));
        System.out.println(strings.stream().collect(Collectors.joining(" ", "Moje ulubione zdanie to: \"", "\".")));
    }


    @Test
    public void collectorsToCollection() {
        LinkedList<String> collect = strings.stream().collect(Collectors.toCollection(LinkedList::new));
    }

    @Test
    public void groupByOldWay() {
        Map<Integer, List<String>> wordsByLength = new HashMap<>();
        for(String word: strings){
            int length = word.length();
            List<String> words = wordsByLength.get(length);
            if(words==null){
                words = new ArrayList<>();
                wordsByLength.put(length, words);
            }
            words.add(word);
        }

        Assertions.assertThat(wordsByLength.get(1)).containsExactlyInAnyOrder("a");
        Assertions.assertThat(wordsByLength.get(2)).containsExactlyInAnyOrder("ma");
        Assertions.assertThat(wordsByLength.get(3)).containsExactlyInAnyOrder("Ala", "jej", "dwa");
        Assertions.assertThat(wordsByLength.get(4)).containsExactlyInAnyOrder("kota", "mama");
        Assertions.assertThat(wordsByLength.get(5)).containsExactlyInAnyOrder("nawet");
    }

    @Test
    public void groupBy() {
        Map<Integer, List<String>> wordsByLength = strings.stream()
                .collect(Collectors.groupingBy(String::length));

        Assertions.assertThat(wordsByLength.get(1)).containsExactlyInAnyOrder("a");
        Assertions.assertThat(wordsByLength.get(2)).containsExactlyInAnyOrder("ma");
        Assertions.assertThat(wordsByLength.get(3)).containsExactlyInAnyOrder("Ala", "jej", "dwa");
        Assertions.assertThat(wordsByLength.get(4)).containsExactlyInAnyOrder("kota", "mama");
        Assertions.assertThat(wordsByLength.get(5)).containsExactlyInAnyOrder("nawet");
    }

    @Test
    public void groupBy2() {
        Map<Integer, Set<String>> wordsByLength = strings.stream()
                .collect(Collectors.groupingBy(String::length, Collectors.toSet()));

        Assertions.assertThat(wordsByLength.get(1)).containsExactlyInAnyOrder("a");
        Assertions.assertThat(wordsByLength.get(2)).containsExactlyInAnyOrder("ma");
        Assertions.assertThat(wordsByLength.get(3)).containsExactlyInAnyOrder("Ala", "jej", "dwa");
        Assertions.assertThat(wordsByLength.get(4)).containsExactlyInAnyOrder("kota", "mama");
        Assertions.assertThat(wordsByLength.get(5)).containsExactlyInAnyOrder("nawet");
    }

    @Test
    public void groupBy3() {
        TreeMap<Integer, Set<String>> wordsByLength = strings.stream()
                .collect(Collectors.groupingBy(String::length, TreeMap::new, Collectors.toSet()));

        Assertions.assertThat(wordsByLength.get(1)).containsExactlyInAnyOrder("a");
        Assertions.assertThat(wordsByLength.get(2)).containsExactlyInAnyOrder("ma");
        Assertions.assertThat(wordsByLength.get(3)).containsExactlyInAnyOrder("Ala", "jej", "dwa");
        Assertions.assertThat(wordsByLength.get(4)).containsExactlyInAnyOrder("kota", "mama");
        Assertions.assertThat(wordsByLength.get(5)).containsExactlyInAnyOrder("nawet");
    }

    @Test
    public void groupBy4() {
        List<Product> products = Arrays.asList(
                new Product(null, "produkt1", null, ProductCategory.ELECTRONIC, null),
                new Product(null, "produkt2", null, ProductCategory.OFFICE, null),
                new Product(null, "produkt3", null, ProductCategory.ELECTRONIC, null)
        );

        Map<ProductCategory, List<String>> collect = products.stream()
                .collect(Collectors.groupingBy(Product::getCategory, Collectors.mapping(Product::getName, Collectors.toList())));

        System.out.println(collect);
    }

    @Test
    public void partitioningBy() {
        Map<Boolean, List<String>> collect = strings.stream().collect(Collectors.partitioningBy(word -> word.length() > 3));
        System.out.println(collect);
    }

    @Test
    public void partitioningBy2() {
        Map<Boolean, Map<Boolean, List<String>>> collect = strings.stream()
                .collect(Collectors.partitioningBy(word -> word.length() > 3,
                        Collectors.partitioningBy(word2 -> word2.length() < 5)));
        System.out.println(collect);
    }

    @Test
    public void groupByRange() {
        Map<Integer, List<String>> result = strings.stream().collect(Collectors.groupingBy(word -> {
            int length = word.length();
            if (length <= 2) {
                return 2;
            }
            if (length <= 4) {
                return 4;
            }
            return 5;
        }));

        System.out.println(result);
    }


    @Test
    public void count() {
        long count = strings.stream().filter(word -> word.length() > 3).count();

        Assertions.assertThat(count).isEqualTo(3);
    }

    @Test
    public void skip() {
        List<String> collect = strings.stream().skip(3).collect(Collectors.toList());

        Assertions.assertThat(collect).containsExactlyInAnyOrder("a", "jej", "mama", "nawet", "dwa");
    }

    @Test
    public void skipAndLimit() {
        List<String> collect = strings.stream()
                .skip(3)
                .limit(3)
                .collect(Collectors.toList());

        Assertions.assertThat(collect).containsExactlyInAnyOrder("a", "jej", "mama");
    }

    @Test
    public void findAny() {
        String firstWord = strings.stream().filter(word -> word.length()>3)
                .findAny()
                .orElse("");

        Assertions.assertThat(firstWord).isEqualTo("kota");
    }

    @Test
    public void distinct() {
        List<String> result = Arrays.asList("Duplikat", "Duplikat", "Nie duplikat").stream()
                .distinct()
                .collect(Collectors.toList());

        Assertions.assertThat(result).containsExactly("Duplikat", "Nie duplikat" );
    }

    @Test
    public void sort() {
        System.out.println(strings.stream().sorted().collect(Collectors.toList()));
        System.out.println(strings.stream().sorted(
                Comparator.comparingInt(String::length)
        ).collect(Collectors.toList()));
        System.out.println(strings.stream().sorted(
                Comparator.comparingInt(String::length).reversed()
        ).collect(Collectors.toList()));
    }
    
    @Test
    public void max() {
        strings.stream().max(Comparator.comparingInt(String::length)).ifPresent(System.out::println);
    }

    @Test
    public void min() {
        strings.stream().min(Comparator.comparingInt(String::length)).ifPresent(System.out::println);
    }

}
